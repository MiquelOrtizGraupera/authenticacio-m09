package com.example.basicaut.model.repositori;

import com.example.basicaut.model.entitats.Videojoc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoriVideojoc extends JpaRepository<Videojoc, Long> {
}
